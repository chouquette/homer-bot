package main

import (
	"fmt"
	"sort"
	"time"
)

// Check is an internal representation of a check
type Check struct {
	Description string
	Success     bool
	Reason      string
	Error       error
}

func (check *Check) baseString() string {
	s := check.Description
	if check.Reason != "" {
		s += " - Reason: " + check.Reason
	}
	if check.Error != nil {
		s += " - ERROR: " + check.Error.Error()
	}
	return s
}

// String provides a string representation of the Check. Used in logs
func (check *Check) String() string {
	if check.Success {
		return "OK  - " + check.baseString()
	}
	return "NOK - " + check.baseString()
}

// Markdown provides a markdown representation of the Check. Used in gitlab comment
func (check *Check) Markdown() string {
	if check.Success {
		return ":ok: - " + check.baseString()
	}
	return ":red_circle: - " + check.baseString()
}

// CheckStatus checks the MR status
func (mr *MergeRequest) CheckStatus() Check {
	return Check{
		"MR should be considered mergeable by Gitlab",
		mr.MergeStatus == MergeStatusCanBeMerged,
		fmt.Sprintf("MR internal status is '%s'", mr.MergeStatus),
		nil,
	}
}

// CheckInactivity checks MR "full" inactivity: no vote, no discussion
func (mr *MergeRequest) CheckInactivity(threshold time.Duration) Check {
	description := fmt.Sprintf("No activity on MR (no review, no vote) and last update is longer than %s", threshold)
	timeSinceLastUpdate := time.Since(mr.UpdatedAt)
	if timeSinceLastUpdate < threshold {
		return Check{description, false, fmt.Sprintf("Last update is %s", timeSinceLastUpdate), nil}
	}
	_, votes, _, err := mr.DevScore(false)
	if err != nil {
		return Check{description, false, "", err}
	}
	if votes > 0 {
		return Check{description, false, fmt.Sprintf("Dev votes have been found (%d)", votes), nil}
	}
	notes, err := mr.GetRelevantThreads()
	if err != nil {
		return Check{description, false, "", err}
	}
	if len(*notes) > 0 {
		return Check{description, false, fmt.Sprintf("Discussion threads have been found (%d)", len(*notes)), nil}
	}
	return Check{description, true, fmt.Sprintf("No vote or thread has been found, and time since last update is %s", timeSinceLastUpdate), nil}
}

// CheckDevScore checks the Developer Votes score.
// threshold is the minimal time before validation
// strict means the score should be strictly positive to be validated
func (mr *MergeRequest) CheckDevScore(threshold time.Duration, strict bool) Check {
	description := fmt.Sprintf("Developers Vote Score is high enough for at least %s, with a minimum of one vote", threshold)
	score, votes, lastSignChange, err := mr.DevScore(strict)
	if err != nil {
		return Check{description, false, "", err}
	}
	if votes == 0 {
		return Check{description, false, "No Developer vote has been found", nil}
	}
	var success bool
	var reason string
	lastSignChangeStr := lastSignChange.String()
	if lastSignChange.IsZero() {
		lastSignChangeStr = "never"
	}
	if strict {
		if score <= 0 {
			success = false
			reason = fmt.Sprintf("Score (%d) is not strictly positive", score)
		} else if time.Since(lastSignChange) < threshold {
			success = false
			reason = fmt.Sprintf("Score (%d) has not been strictly positive long enough (last sign change: %s)", score, lastSignChangeStr)
		} else {
			success = true
			reason = fmt.Sprintf("Score (%d) has been strictly positive long enough (last sign change: %s)", score, lastSignChangeStr)
		}
	} else {
		if score < 0 {
			success = false
			reason = fmt.Sprintf("Score (%d) is strictly negative", score)
		} else if time.Since(lastSignChange) < threshold {
			success = false
			reason = fmt.Sprintf("Score (%d) has not been positive long enough (last sign change: %s)", score, lastSignChangeStr)
		} else {
			success = true
			reason = fmt.Sprintf("Score (%d) has been positive long enough (last sign change: %s)", score, lastSignChangeStr)
		}
	}
	return Check{description, success, reason, nil}
}

// CheckDiscussions checks all threads in MR have been resolved
// threshold is the minimal time before validation
func (mr *MergeRequest) CheckDiscussions(threshold time.Duration) Check {
	description := fmt.Sprintf("All Threads should be resolved for at least %s", threshold)
	notes, err := mr.GetRelevantThreads()
	if err != nil {
		return Check{description, false, "", err}
	}
	lastResolved := time.Time{}
	for _, note := range *notes {
		if !note.Resolved {
			return Check{description, false, fmt.Sprintf("A thread is still [unresolved](/-/merge_requests/%d#note_%d)", mr.IID, note.ID), nil}
		}
		if note.Author.ID != mr.Author.ID && note.ResolvedBy != nil && note.ResolvedBy.ID == note.Author.ID {
			// NOTE: should we do something else ? A "Invalid" label on the MR ?
			return Check{description, false, fmt.Sprintf("A thread not started by the MR Author has been closed by the MR Author: [thread](/-/merge_requests/%d#note_%d)", mr.IID, note.ID), nil}
		}
		// resolved + legit => take updatetime into account
		if note.UpdatedAt.After(lastResolved) {
			lastResolved = note.UpdatedAt
		}
	}
	if time.Since(lastResolved) < threshold {
		return Check{description, false, fmt.Sprintf("Threads have not been resolved for enough time (last resolved: %s)", lastResolved), nil}
	}
	return Check{
		description,
		true,
		"No unresolved thread has been found",
		nil,
	}
}

// CheckPipelines checks last pipeline of MR is successful
func (mr *MergeRequest) CheckPipelines() Check {
	description := "Last pipeline should be successful"
	pipelines, err := mr.GetPipelines()
	if err != nil {
		return Check{description, false, "", err}
	}
	if len(pipelines) == 0 {
		return Check{description, false, "No pipeline has been found", nil}
	}
	// sort pipelines by update time DESK
	sort.Slice(pipelines, func(i, j int) bool {
		return pipelines[i].UpdatedAt.After(pipelines[j].UpdatedAt)
	})
	lastPipeline := pipelines[0]
	return Check{
		description,
		lastPipeline.Status == PipelineStatusSuccess,
		fmt.Sprintf("Last Pipeline (%d) has status %s", lastPipeline.ID, lastPipeline.Status),
		nil,
	}
}

// CheckAuthorIsDev checks if the author has a developer access or higher
// This is an optional check that is used to validate the "Inactivity" scenario
func (mr *MergeRequest) CheckAuthorIsDev() Check {
	description := "Check whether MR author has developer access or higher"
	authorIsDev, err := mr.IsAuthorDevOrHigher()
	if err != nil {
		return Check{description, false, "", err}
	}
	if authorIsDev {
		return Check{description, true, "Author has developer access or higher", nil}
	}
	return Check{description, false, "Author does NOT have developer access or higher", nil}
}

// Analyze generates a the analysis report of the MR
// A "report" is a list of checks
func (mr *MergeRequest) Analyze() []Check {
	checks := []Check{}
	// MANDATORY checks
	checks = append(checks, mr.CheckStatus())
	checks = append(checks, mr.CheckPipelines())
	// Checks that depend on author role (>= developer or not)
	devCheck := mr.CheckAuthorIsDev()
	if devCheck.Error != nil {
		// Unable to determine the author role, aborting.
		checks = append(checks, devCheck)
		return checks
	}
	if devCheck.Success {
		// Author is >= developer
		checks = append(checks, devCheck)
		// Special Case: inactivity on a developer MR
		inactiveCheck := mr.CheckInactivity(time.Duration(mrInactiveHours) * time.Hour)
		if inactiveCheck.Success {
			checks = append(checks, inactiveCheck)
			return checks
		}
		logMRVerbose(mr.IID, "[OPTIONAL] ", inactiveCheck.String())
		checks = append(checks, mr.CheckDevScore(time.Duration(mrDevScoreHours)*time.Hour, false))
		checks = append(checks, mr.CheckDiscussions(time.Duration(mrDevDiscussionHours)*time.Hour))
	} else {
		checks = append(checks, mr.CheckDevScore(time.Duration(mrOtherScoreHours)*time.Hour, true))
		checks = append(checks, mr.CheckDiscussions(time.Duration(mrOtherDiscussionHours)*time.Hour))
	}
	return checks
}
