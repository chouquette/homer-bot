package main

import (
	"fmt"
	"net/http"
	"sort"
	"sync"
	"time"
)

// Constants from the Gitlab API.
const (
	AccessLevelDeveloper = 30
	//AccessLevelMaintainer  = 40
	//AccessLevelOwner       = 50
	VoteUp                 = "thumbsup"
	VoteDown               = "thumbsdown"
	MergeStatusCanBeMerged = "can_be_merged"
	PipelineStatusSuccess  = "success"
)

// User is a Gitlab representation of a user
type User struct {
	ID          int64
	Name        string
	Username    string
	AccessLevel int64 `json:"access_level"` // Only when retrieve members of a project
	// state, avatar_url, web_url seem unrelevant
}

// AwardEmoji is a Gitlab AwardEmoji
type AwardEmoji struct {
	ID            int64
	Name          string
	User          User
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	AwardableID   int64
	AwardableType string
}

// TaskCompletionStatus is a Gitlab task completion status
type TaskCompletionStatus struct {
	Count          int64 `json:"count"`
	CompletedCount int64 `json:"completed_count"`
}

// Discussion is a Gitlab Discussion
type Discussion struct {
	ID             string
	IndividualNote bool   `json:"individual_note"`
	Notes          []Note `json:"notes"`
}

// Note is a Gitlab Discussion Note
type Note struct {
	ID         int64
	Type       string
	Body       string
	Author     User
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	Resolvable bool
	Resolved   bool
	ResolvedBy *User `json:"resolved_by"`
}

// Pipeline is a Gitlab Pipeline
type Pipeline struct {
	ID        int64
	Sha       string
	Status    string
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// MergeRequest is a Gitlab MergeRequest
type MergeRequest struct {
	ID                          int64
	IID                         int64 `json:"iid"`
	Title                       string
	Upvotes                     int64
	Downvotes                   int64
	Author                      User
	Labels                      []string             // comma separated list of labels
	WorkInProgress              bool                 `json:"work_in_progress"`
	MergeStatus                 string               `json:"merge_status"` // == "can_be_merged" ?
	CreatedAt                   time.Time            `json:"created_at"`
	UpdatedAt                   time.Time            `json:"updated_at"`
	DiscussionLocked            bool                 `json:"discussion_locked"`
	WebURL                      string               `json:"web_url"`
	TaskCompletionStatus        TaskCompletionStatus `json:"task_completion_status"`
	HasConflicts                bool                 `json:"has_conflicts"`
	BlockingDiscussionsResolved bool                 `json:"blocking_discussions_resolved"`
	// Non gitlab fields
	Project     *Project      `json:"-"`
	awardEmojis *[]AwardEmoji // request cache
	discussions *[]Discussion // request cache
}

// GetPipelines retrieves pipelines related to a MR
func (mr *MergeRequest) GetPipelines() ([]Pipeline, error) {
	var pipelines []Pipeline
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/pipelines`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &pipelines); err != nil {
		return pipelines, err
	}
	return pipelines, nil
}

// GetAwardEmojis retrieves emojis "awarded" (added) an MR
// NOTE: assuming this will be accessed SEQUENTIALLY per MergeRequest
// This will not be "updated" during the whole MR object lifetime
func (mr *MergeRequest) GetAwardEmojis() (*[]AwardEmoji, error) {
	if mr.awardEmojis != nil {
		return mr.awardEmojis, nil
	}
	var emojis []AwardEmoji
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/award_emoji`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &emojis); err != nil {
		return nil, err
	}
	// sort emojis by update time
	sort.Slice(emojis, func(i, j int) bool {
		return emojis[i].UpdatedAt.Before(emojis[j].UpdatedAt)
	})
	mr.awardEmojis = &emojis
	return mr.awardEmojis, nil
}

// GetDiscussions retrieves all the discussions included in a MR
// NOTE: assuming this will be accessed SEQUENTIALLY per MergeRequest
// This will not be "updated" during the whole MR object lifetime
func (mr *MergeRequest) GetDiscussions() (*[]Discussion, error) {
	if mr.discussions != nil {
		return mr.discussions, nil
	}
	var discussions []Discussion
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/discussions`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &discussions); err != nil {
		return nil, err
	}
	mr.discussions = &discussions
	return mr.discussions, nil
}

// GetRelevantThreads get all gitlab "Notes" aka discussion threads item that are relevant:
// - For each discussion,
// - a note must be "resolvable": this is not a simple commentary, this is a review/thread
func (mr *MergeRequest) GetRelevantThreads() (*[]Note, error) {
	notes := []Note{}
	discussions, err := mr.GetDiscussions()
	if err != nil {
		return nil, err
	}
	for _, discussion := range *discussions {
		for _, note := range discussion.Notes {
			if note.Resolvable {
				notes = append(notes, note)
			}
		}
	}
	return &notes, nil
}

// IsAuthorDevOrHigher checks whether the author of the MR has Developer role access level or higher in the project
func (mr *MergeRequest) IsAuthorDevOrHigher() (bool, error) {
	devs, err := mr.Project.GetDevOrHigherByID()
	if err != nil {
		return false, err
	}
	_, ok := devs[mr.Author.ID]
	return ok, nil
}

// DevScore computes a Developer Score by checking the emoji awards (aka "thumbs up/down").
// Only users with an access level higher or equal to Developer are taken into account.
// Your own vote does not count.
// Returns:
// - the score
// - the number of votes
// - the last time the score has changed its sign, or Epoch if undefined.
// - an error if any
// strictly == true means the function returns the last time the score has become < 1 or >= 1
// strictly == false means the function returns the last time the score has become < 0 or >= 0
func (mr *MergeRequest) DevScore(strictly bool) (int64, int64, time.Time, error) {
	t := time.Time{} // Epoch
	devs, err := mr.Project.GetDevOrHigherByID()
	if err != nil {
		return 0, 0, t, err
	}
	emojis, err := mr.GetAwardEmojis()
	if err != nil {
		return 0, 0, t, err
	}
	var score int64 = 0
	var votes int64 = 0
	for _, award := range *emojis {
		logMRVerbose(mr.IID, "Emoji ", award.Name, award.UpdatedAt)
		if award.User.ID == mr.Author.ID {
			continue // your own vote does not count
		}
		if _, ok := devs[award.User.ID]; !ok {
			continue // not a dev
		}
		switch emoji := award.Name; emoji {
		case VoteUp:
			score++
			votes++
			if (score == 1 && strictly) || (score == 0 && !strictly) {
				t = award.UpdatedAt
			}
		case VoteDown:
			score--
			votes++
			if (score == 0 && strictly) || (score == -1 && !strictly) {
				t = award.UpdatedAt
			}
		default:
			logMRVerbose(mr.IID, emoji, "is not an emoji that counts as a vote")
		}
	}
	return score, votes, t, nil
}

// CreateNote add a comment in the MR discussion
func (mr *MergeRequest) CreateNote(body string) error {
	var createNote struct {
		Body string `json:"body"`
	}
	createNote.Body = body
	if err := requestJSON(http.MethodPost, fmt.Sprintf(`%s/notes`, mr.GetAPIURL()), map[string]string{}, createNote, mr.Project.AuthToken, nil); err != nil {
		return err
	}
	return nil
}

// HasLabel checks whether the MR has a label associated
func (mr *MergeRequest) HasLabel(label string) bool {
	for _, l := range mr.Labels {
		if l == label {
			return true
		}
	}
	return false
}

// AddLabel adds a given label to the MR
func (mr *MergeRequest) AddLabel(label string) error {
	var addLabel struct {
		AddLabels string `json:"add_labels"`
	}
	addLabel.AddLabels = label
	if err := requestJSON(http.MethodPut, mr.GetAPIURL(), map[string]string{}, addLabel, mr.Project.AuthToken, nil); err != nil {
		return err
	}
	return nil
}

// RemoveLabel removes a given label to the MR
func (mr *MergeRequest) RemoveLabel(label string) error {
	var removeLabel struct {
		RemoveLabels string `json:"remove_labels"`
	}
	removeLabel.RemoveLabels = label
	if err := requestJSON(http.MethodPut, mr.GetAPIURL(), map[string]string{}, removeLabel, mr.Project.AuthToken, nil); err != nil {
		return err
	}
	return nil
}

// GetAPIURL provides the MR API root URL
func (mr *MergeRequest) GetAPIURL() string {
	return fmt.Sprintf(`%s/merge_requests/%d`, mr.Project.GetAPIURL(), mr.IID)
}

// Project is an internal representation of a Project
// This is NOT a Gitlab project, only an easy way of storing project connection information
type Project struct {
	ID         int64
	APIRootURL string
	AuthToken  string
	members    *[]User // Members request Cache.
	membersMu  sync.Mutex
}

// GetAPIURL provides the Project API root URL
func (p *Project) GetAPIURL() string {
	return fmt.Sprintf(`%s/projects/%d`, p.APIRootURL, p.ID)
}

// GetMRs retrieves all the MRs inside the project, with given optional filters
func (p *Project) GetMRs(filters map[string]string) ([]MergeRequest, error) {
	var mrs []MergeRequest
	url := fmt.Sprintf(`%s/merge_requests`, p.GetAPIURL())
	if err := requestJSON(http.MethodGet, url, filters, "", p.AuthToken, &mrs); err != nil {
		return mrs, err
	}
	for index := range mrs {
		mrs[index].Project = p
	}
	return mrs, nil
}

// GetAllMembers retrieves project members, including inherited members
// The request will be performed only once if it does not fail
// NOTE: there seem to be no way to filter this list in gitlab API
func (p *Project) GetAllMembers() (*[]User, error) {
	// This "cache" will be used in all MergeRequest goroutines, so we probably need to be safe
	p.membersMu.Lock()
	defer p.membersMu.Unlock()
	if p.members != nil {
		return p.members, nil
	}
	var members []User
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/projects/%d/members/all`, p.APIRootURL, p.ID), map[string]string{}, "", p.AuthToken, &members); err != nil {
		return nil, err
	}
	p.members = &members
	return p.members, nil
}

// GetDevOrHigherByID provides all devs who have an access level higher or equal to a Developer
// Returns a ID -> User map
func (p *Project) GetDevOrHigherByID() (map[int64]User, error) {
	members, err := p.GetAllMembers()
	if err != nil {
		return nil, err
	}
	devs := map[int64]User{}
	for _, member := range *members {
		if member.AccessLevel >= AccessLevelDeveloper {
			devs[member.ID] = member
		}
	}
	return devs, nil
}
